@echo off
:Ask
echo Push to UAT or Prod (U/P)
set INPUT=
set /P INPUT=Type input: %=%
If /I "%INPUT%"=="U" goto uat 
If /I "%INPUT%"=="u" goto uat 
If /I "%INPUT%"=="P" goto prod
If /I "%INPUT%"=="p" goto prod
echo Incorrect input
goto end
:uat
mvn package -P uat & cf push
echo "go uat"
goto end
:prod
mvn package -P prod & cf push
echo "go prod"
goto end
:end