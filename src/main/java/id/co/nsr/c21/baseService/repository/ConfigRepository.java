package id.co.nsr.c21.baseService.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nsr.c21.baseService.domain.Config;

/**
 * JpaRepository extends PagingAndSortingRepository which in turn extends
 * CrudRepository.
 * 
 * Their main functions are:
 * <ul>
 * <li>CrudRepository mainly provides CRUD functions.</li>
 * <li>PagingAndSortingRepository provides methods to do pagination and sorting
 * records.</li>
 * <li>JpaRepository provides some JPA-related methods such as flushing the
 * persistence context and deleting records in a batch.
 * <li>
 *
 */
public interface ConfigRepository extends JpaRepository<Config, Long> {

}