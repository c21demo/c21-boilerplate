package id.co.nsr.c21.baseService.web.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.nsr.c21.baseService.web.api.model.Response;

@Service
public class ListVersionsApiDelegateImpl implements ListVersionsApiDelegate {

    public ResponseEntity<Response> listVersions() {
    	
    	Response resp = new Response();
    	resp.setCode(123);
    	resp.setMessage("Hello there!");
    	
    	return new ResponseEntity<Response>(resp, HttpStatus.OK);
    }
}
