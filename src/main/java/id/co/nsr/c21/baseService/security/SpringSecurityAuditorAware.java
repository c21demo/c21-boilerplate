package id.co.nsr.c21.baseService.security;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import id.co.nsr.c21.commons.security.SecurityUtils;
import id.co.nsr.c21.commons.util.AppConstants;

/**
 * Implementation of AuditorAware based on Spring Security.
 */
@Component
public class SpringSecurityAuditorAware implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of(SecurityUtils.getCurrentUserLogin().orElse(AppConstants.ACCOUNT_SYSTEM));
    }
}
