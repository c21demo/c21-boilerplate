package id.co.nsr.c21.baseService.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nsr.c21.baseService.domain.Config;
import id.co.nsr.c21.baseService.repository.ConfigRepository;
import id.co.nsr.c21.commons.web.rest.errors.BadRequestAlertException;
import id.co.nsr.c21.commons.web.rest.util.HeaderUtils;
import id.co.nsr.c21.commons.web.rest.util.PaginationUtil;
import id.co.nsr.c21.commons.web.util.ResponseUtils;
import io.micrometer.core.annotation.Timed;

/**
 * REST controller for managing Address.
 */
@RestController
@RequestMapping("/api")
public class ConfigResource {

    private final Logger LOGGER = LoggerFactory.getLogger(ConfigResource.class);

    private static final String ENTITY_NAME = "config";

    private final ConfigRepository configRepository;

    public ConfigResource(ConfigRepository configRepository) {
        this.configRepository = configRepository;
    }

    /**
     * POST  /configs : Create a new config.
     *
     * @param config the config to create
     * @return the ResponseEntity with status 201 (Created) and with body the new config, or with status 400 (Bad Request) if the config has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/configs")
    @Timed
    public ResponseEntity<Config> createConfig(@Valid @RequestBody Config config) throws URISyntaxException {
        LOGGER.debug("REST request to save Config : {}", config);
        if (config.getId() != null) {
            throw new BadRequestAlertException("A new config cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Config result = configRepository.save(config);
        return ResponseEntity.created(new URI("/api/configs/" + result.getId()))
            .headers(HeaderUtils.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /configs : Updates an existing config.
     *
     * @param config the config to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated config,
     * or with status 400 (Bad Request) if the config is not valid,
     * or with status 500 (Internal Server Error) if the config couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/configs")
    @Timed
    public ResponseEntity<Config> updateConfig(@Valid @RequestBody Config config) throws URISyntaxException {
        LOGGER.debug("REST request to update Config : {}", config);
        if (config.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Config result = configRepository.save(config);
        return ResponseEntity.ok()
            .headers(HeaderUtils.createEntityUpdateAlert(ENTITY_NAME, config.getId().toString()))
            .body(result);
    }

    /**
     * GET  /configs : get all the configs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of configs in body
     */
    @GetMapping("/configs")
    @Timed
    public ResponseEntity<List<Config>> getAllConfigs(Pageable pageable) {
        LOGGER.debug("REST request to get a page of Configs");
        Page<Config> page = configRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/configs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /addresses/:id : get the "id" address.
     *
     * @param id the id of the address to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the address, or with status 404 (Not Found)
     */
    @GetMapping("/configs/{id}")
    @Timed
    public ResponseEntity<Config> getConfig(@PathVariable Long id) {
        LOGGER.debug("REST request to get Config : {}", id);
        Optional<Config> config = configRepository.findById(id);
        return ResponseUtils.wrapOrNotFound(config);
    }

    /**
     * DELETE  /addresses/:id : delete the "id" config.
     *
     * @param id the id of the config to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/configs/{id}")
    @Timed
    public ResponseEntity<Void> deleteConfig(@PathVariable Long id) {
        LOGGER.debug("REST request to delete Config : {}", id);
        configRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtils.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
