package id.co.nsr.c21.baseService.web.rest.errors;

import org.springframework.web.bind.annotation.ControllerAdvice;

import id.co.nsr.c21.commons.web.rest.errors.ExceptionTranslator;

@ControllerAdvice
public class DefaultExceptionTranslator extends ExceptionTranslator {

}
