package id.co.nsr.c21.baseService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.core.env.Environment;

import id.co.nsr.c21.commons.util.AppUtils;
import id.co.nsr.c21.commons.util.DefaultProfileUtil;

@SpringBootApplication
@EnableDiscoveryClient
@EnableAutoConfiguration(exclude = { SecurityAutoConfiguration.class })
public class C21BoilerplateApp {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(C21BoilerplateApp.class);
		DefaultProfileUtil.addDefaultProfile(app);
        Environment env = app.run(args).getEnvironment();
        AppUtils.logApplicationStartup(env);
	}
	
	
}
