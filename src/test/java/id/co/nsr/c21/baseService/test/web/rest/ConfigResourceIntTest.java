package id.co.nsr.c21.baseService.test.web.rest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import id.co.nsr.c21.baseService.C21BoilerplateApp;
import id.co.nsr.c21.baseService.domain.Config;
import id.co.nsr.c21.baseService.repository.ConfigRepository;
import id.co.nsr.c21.baseService.web.rest.ConfigResource;
import id.co.nsr.c21.commons.web.rest.errors.ExceptionTranslator;


/**
 * Test class for the ConfigResource REST controller.
 *
 * @see ConfigResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = C21BoilerplateApp.class)

public class ConfigResourceIntTest {
	
	private final Logger LOGGER = LoggerFactory.getLogger(ConfigResourceIntTest.class);

    private static final String CONFIG_NAME_1 = "message.motd";
    private static final String CONFIG_VALUE_1 = "Welcome!";
	
	@Autowired
	private ConfigRepository configRepository;

//	@Autowired
//	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	private MockMvc restConfigMockMvc;

	private Config config;

	@Before
	public void setup() {
		
		LOGGER.debug("Setup starting");
		
		MockitoAnnotations.initMocks(this);
		final ConfigResource configResource = new ConfigResource(configRepository);
		
//		this.restConfigMockMvc = MockMvcBuilders.standaloneSetup(configRepository)
//				.setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
//				.setConversionService(TestUtils.createFormattingConversionService())
//				.setMessageConverters(mapJacksonMessageConverter).build();
		
		this.restConfigMockMvc = MockMvcBuilders.standaloneSetup(configResource)
				.setCustomArgumentResolvers(pageableArgumentResolver)
				.setControllerAdvice(exceptionTranslator)
				.setConversionService(TestUtils.createFormattingConversionService())
				.build();
	}

	public static Config createEntity(EntityManager em) {
		Config config = new Config();
		config.setName(CONFIG_NAME_1);
		config.setValue(CONFIG_VALUE_1);
		return config;
	}
	
    @Before
    public void initTest() {
        config = createEntity(em);
    }

    @Test
    @Transactional
    public void createConfig() throws Exception {
        int databaseSizeBeforeCreate = configRepository.findAll().size();

        // Create the Config
        restConfigMockMvc.perform(MockMvcRequestBuilders.post("/api/configs")
            .contentType(TestUtils.APPLICATION_JSON_UTF8)
            .content(TestUtils.convertObjectToJsonBytes(config)))
            .andExpect(MockMvcResultMatchers.status().isCreated());

        // Validate the Config in the database
        List<Config> configList = configRepository.findAll();
        assertThat(configList).hasSize(databaseSizeBeforeCreate + 1);
        Config testConfig = configList.get(configList.size() - 1);
        assertThat(testConfig.getName()).isEqualTo(CONFIG_NAME_1);
        assertThat(testConfig.getValue()).isEqualTo(CONFIG_VALUE_1);
    }

}
