
# Table of Contents
* [Service Registry](#serviceregistry)
* [Pushing to Cloud Foundry](#pcfdeployment)
* [Doing API-First development](#openapi)
* [Running Unit Tests](#tests)


### Service Registry<a name="serviceregistry"></a>

Running against local standalone Eureka Server (Dev profile)
![Eureka Service Registry](assets/eureka-service-registry.png)

Running against PCF Service Registry (SIT profile)
![PCF Service Registry](assets/p.service-registry.png)

### Pushing to Cloud Foundry<a name="pcfdeployment"></a>

Use **sit** spring profile to push to PCF. Refer to pom.xml for the dependencies that are included as part of **sit**, and compare with **dev** profile to understand the difference.

```
$ mvn package -Psit # repackage for CF (e.g. will use mysql instead of h2)
$ cf push
PS \xxx\yyy\c21\c21demo\c21-boilerplate> cf push
Pushing from manifest to org demo / space apps as demo-user...
Using manifest file C:\dev\workspaces\c21\c21demo\c21-boilerplate\manifest.yml
Getting app info...
Creating app with these attributes...
+ name:        c21-boilerplate
  path:        \xxx\yyy\c21\c21demo\c21-boilerplate\target\c21-boilerplate-1.0.0-SNAPSHOT.jar
+ instances:   1
+ memory:      1G
  services:
+   c21-service-registry
  env:
+   SPRING_PROFILES_ACTIVE
  routes:
+   c21-boilerplate.apps.pcf-dev.int.21cineplex.com
Creating app c21-boilerplate...
```

The deployment will fail so run the following commands

```
$ cf create-service p.mysql db-small c21-boilerplate-db
$ cf bind-service c21-boilerplate c21-boilerplate-db
```

```
$ cf create-service p-service-registry standard c21-service-registry
$ cf bind-service c21-boilerplate c21-service-registry
$ cf set-env c21-boilerplate TRUST_CERTS api.sys.pcf-dev.int.21cineplex.com
```

```
$ cf restage c21-boilerplate
```

### Doing API-First development using openapi-generator<a name="openapi"></a>

OpenAPI-Generator is configured for this application. You can generate API code from the `src/main/resources/swagger/api.yml` definition file by running:

```
$ ./mvnw generate-sources
```

### Running Unit Tests<a name="tests"></a>


```
$ ./mvnw test
```
